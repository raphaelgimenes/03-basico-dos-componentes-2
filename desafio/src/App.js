import React, { Component } from 'react'
import Input from './Input'
import NumberPrice from './NumberPrice'
import FormattedPrice from './FormattedPrice'
import WelcomeDialog from './WelcomeDialog'
import './App.css'

class App extends Component {
	state = {
		firstName: 'FatecLab',
		price: 0
    }

    changePrice = value => {
        this.setState({ price: value })
    }

	render() {
		return (
			<div className="App">
				<WelcomeDialog firstName={this.state.firstName}/>
				<Input changePrice={this.changePrice}/>
				<NumberPrice price={this.state.price}/>
                <FormattedPrice price={this.state.price} />				
			</div>
		)
	}
}

export default App